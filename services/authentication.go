package services

import (
	dbConnection "../../db"
	"../models"
	"crypto/rsa"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"io/ioutil"
	"log"
	"time"
)

var (
	privateKey *rsa.PrivateKey
	publicKey  *rsa.PublicKey
)

func init() {
	privateBytes, err := ioutil.ReadFile("./core/keys/private.rsa")
	if err != nil {
		log.Fatal("Can't read private file")
	}

	publicBytes, err := ioutil.ReadFile("./core/keys/public.rsa.pub")
	if err != nil {
		log.Fatal("Can't read public file")
	}

	privateKey, err = jwt.ParseRSAPrivateKeyFromPEM(privateBytes)
	if err != nil {
		log.Fatal("Don't parse the private key")
	}
	publicKey, err = jwt.ParseRSAPublicKeyFromPEM(publicBytes)
	if err != nil {
		log.Fatal("Don't parse the public key")
	}
}

func GenerateJWT(user models.User) string {
	claims := models.Claim{
		User: user,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 1).Unix(),
			Issuer: "new token",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	result, err := token.SignedString(privateKey)
	if err != nil {
		log.Fatal("Don't could sign")
	}
	return result
}

func LoginUser(user models.User) (*models.ResponseToken, error) {
	var userDB models.User
	db, err := dbConnection.GetDB()
	if err != nil {
		return nil, err
	}
	row := db.QueryRow("SELECT username, password FROM users WHERE username = ?", user.Username)
	err = row.Scan(&userDB.Username, &userDB.Password)
	if err != nil {
		return nil, err
	}
	var result models.ResponseToken
	if user.Username == userDB.Username {
		err = bcrypt.CompareHashAndPassword([]byte(userDB.Password), []byte(user.Password))
		if err != nil {
			return nil, err
		}
		user.Password = ""
		user.Role = "admin"
		token := GenerateJWT(user)
		result = models.ResponseToken{Token: token}
	}

	return &result, nil
}
